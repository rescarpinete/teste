package br.com.teste.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.teste.entity.TabClienteEndereco;
import br.com.teste.json.CepImpJson;
import br.com.teste.json.CepJson;
import br.com.teste.json.RespostaCepJson;
import br.com.teste.util.JpaUtil;


public class ConsultaEnderecoServices {

	

	public RespostaCepJson consultaCepServices(CepJson cep, CepImpJson  cepJson){
		
	RespostaCepJson resposta = new RespostaCepJson();
		
		String[] ceps =  {"181320","1813200","18132000"}; 
		
		 cepJson.setCep("18132000");
		 cepJson.setRua("Governador Carvalho Pinto");
		 cepJson.setBairro("Jardim Boa Vista");
		 cepJson.setCidade("São Roque");
		 cepJson.setEstado("São Paulo");
		
		List<CepImpJson> cepListJson= new ArrayList<CepImpJson>();
		
        if(cep.getCep().equals( cepJson.getCep()) ){
        	cepListJson.add( cepJson);
			resposta.setFretes(cepListJson);
        }else{
        	for(int i=0; i < ceps.length ; i++){					
				if(cep.getCep().contains(ceps[i])) {
	              if(cep.getCep().equals( cepJson.getCep()) ){
	            	  cepListJson.add( cepJson);
	  				resposta.setFretes(cepListJson);
	  				break;
				  }else{
					cep.setCep(cep.getCep().concat("0"));
				  }
				}else{
					String msg = "CEP Invalido";
					resposta.setMsg(msg);
					break;
				}
			}
        }
		return resposta;
	}
	
	
	public RespostaCepJson enviaEnderecoMock(CepImpJson cepJson){		
		
		EntityManager em = JpaUtil.getEntityManager();
		
		RespostaCepJson resp = new RespostaCepJson();		
		
		TabClienteEndereco endereco = new TabClienteEndereco();
		try {
		
			endereco = em.find(TabClienteEndereco.class, 1);
						
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
		
		
		
		return resp;
	}
}
