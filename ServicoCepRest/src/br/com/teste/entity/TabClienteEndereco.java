package br.com.teste.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TAB_CLIENTE_ENDERECO")
public class TabClienteEndereco implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="CLI_END_ID")
	private int cliEndId;
	

	@Column(name="CLI_END_RUA") 
	private String cliEndRua;
	
	@Column(name="CLI_END_NUMERO") 
	private int cliEndNumero;
		
	@Column(name="CLI_END_CEP")
	private int cliEndCep;
	
	@Column(name="CLI_END_CIDADE") 
	private String cliEndCidade;
	
	@Column(name="CLI_END_ESTADO") 
	private String cliEndEstado;
	
	@Column(name="CLI_END_BAIRRO") 
	private String cliEndBairro;
	
	@Column(name="CLI_END_COMPLEMENTO") 
	private String cliEndComplemento;
		
	/**
	 * @return the cliEndId
	 */
	public int getCliEndId() {
		return cliEndId;
	}

	/**
	 * @param cliEndId the cliEndId to set
	 */
	public void setCliEndId(int cliEndId) {
		this.cliEndId = cliEndId;
	}


	/**
	 * @return the cliEndRua
	 */
	public String getCliEndRua() {
		return cliEndRua;
	}

	/**
	 * @param cliEndRua the cliEndRua to set
	 */
	public void setCliEndRua(String cliEndRua) {
		this.cliEndRua = cliEndRua;
	}

	/**
	 * @return the cliEndNumero
	 */
	public int getCliEndNumero() {
		return cliEndNumero;
	}

	/**
	 * @param cliEndNumero the cliEndNumero to set
	 */
	public void setCliEndNumero(int cliEndNumero) {
		this.cliEndNumero = cliEndNumero;
	}

	/**
	 * @return the cliEndCep
	 */
	public int getCliEndCep() {
		return cliEndCep;
	}

	/**
	 * @param cliEndCep the cliEndCep to set
	 */
	public void setCliEndCep(int cliEndCep) {
		this.cliEndCep = cliEndCep;
	}

	/**
	 * @return the cliEndCidade
	 */
	public String getCliEndCidade() {
		return cliEndCidade;
	}

	/**
	 * @param cliEndCidade the cliEndCidade to set
	 */
	public void setCliEndCidade(String cliEndCidade) {
		this.cliEndCidade = cliEndCidade;
	}

	/**
	 * @return the cliEndEstado
	 */
	public String getCliEndEstado() {
		return cliEndEstado;
	}

	/**
	 * @param cliEndEstado the cliEndEstado to set
	 */
	public void setCliEndEstado(String cliEndEstado) {
		this.cliEndEstado = cliEndEstado;
	}

	/**
	 * @return the cliEndBairro
	 */
	public String getCliEndBairro() {
		return cliEndBairro;
	}

	/**
	 * @param cliEndBairro the cliEndBairro to set
	 */
	public void setCliEndBairro(String cliEndBairro) {
		this.cliEndBairro = cliEndBairro;
	}

	/**
	 * @return the cliEndComplemento
	 */
	public String getCliEndComplemento() {
		return cliEndComplemento;
	}

	/**
	 * @param cliEndComplemento the cliEndComplemento to set
	 */
	public void setCliEndComplemento(String cliEndComplemento) {
		this.cliEndComplemento = cliEndComplemento;
	}


}
