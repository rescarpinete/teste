package br.com.teste.facade;

import br.com.teste.json.CepImpJson;
import br.com.teste.json.RespostaCepJson;


public interface ConsultaCepFacadeLocal {

	RespostaCepJson enviaEnderecoMock(CepImpJson cepJson);

	

}
