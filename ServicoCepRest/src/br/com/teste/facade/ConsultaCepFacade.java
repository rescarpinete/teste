package br.com.teste.facade;

import br.com.teste.json.CepImpJson;
import br.com.teste.json.CepJson;
import br.com.teste.json.RespostaCepJson;
import br.com.teste.services.ConsultaEnderecoServices;


public class ConsultaCepFacade implements ConsultaCepFacadeLocal{
	

	
	
	public RespostaCepJson consultaFrete(CepJson cep, CepImpJson cepJson){
		ConsultaEnderecoServices consultaEnderecoServices = new ConsultaEnderecoServices();
		return consultaEnderecoServices.consultaCepServices(cep, cepJson);
	}

	
	
	public RespostaCepJson enviaEnderecoMock(CepImpJson  cepJson){
		ConsultaEnderecoServices consultaFreteSession = new ConsultaEnderecoServices();	
		return consultaFreteSession.enviaEnderecoMock( cepJson);
	}



	
}
