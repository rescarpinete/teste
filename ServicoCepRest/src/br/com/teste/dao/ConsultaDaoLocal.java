package br.com.teste.dao;

import br.com.teste.json.CepImpJson;

public interface ConsultaDaoLocal{

	public void createEndereco(CepImpJson cepJson);

}
