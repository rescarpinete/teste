package br.com.teste.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.teste.facade.ConsultaCepFacade;
import br.com.teste.facade.ConsultaCepFacadeLocal;
import br.com.teste.json.CepImpJson;
import br.com.teste.json.CepJson;
import br.com.teste.json.RespostaCepJson;
	

@Path("/endereco")
public class ServicoEndereco {
	
	//Questao 1
	@POST
	@Path("/consultaCep/mock")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public RespostaCepJson consultaFreteMock(CepJson cep){
		RespostaCepJson resposta = new RespostaCepJson();
		ConsultaCepFacade facade = new ConsultaCepFacade();
		try {			
			
			CepImpJson frete = new CepImpJson();
			
			resposta = facade.consultaFrete(cep,frete);
			
		} catch (Exception e) {
			System.out.println("Erro - " + e.getStackTrace());
		}	
		
		return resposta;		
	}
	
	//Questao 2 
	@POST
	@Path("/enviaEndereco/mock")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public RespostaCepJson enviaEnderecoMock(CepImpJson cepJson){
		RespostaCepJson resposta = new RespostaCepJson();
		ConsultaCepFacadeLocal consultaCepFacade = new ConsultaCepFacade();
		try {	
			
			resposta = consultaCepFacade.enviaEnderecoMock(cepJson);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro - " + e.getStackTrace());
		}	
		
		return resposta;		
	}
}
