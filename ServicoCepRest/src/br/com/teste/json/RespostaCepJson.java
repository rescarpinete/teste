package br.com.teste.json;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RespostaCepJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private List<CepImpJson> fretes;
	
	private String msg;


	/**
	 * @return the fretes
	 */
	public List<CepImpJson> getFretes() {
		return fretes;
	}


	/**
	 * @param fretes the fretes to set
	 */
	public void setFretes(List<CepImpJson> fretes) {
		this.fretes = fretes;
	}


	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}


	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	

}
