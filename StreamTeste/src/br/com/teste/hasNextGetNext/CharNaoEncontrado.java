package br.com.teste.hasNextGetNext;

public class CharNaoEncontrado extends Exception {

	private static final long serialVersionUID = 1L;

	public CharNaoEncontrado() {
    	super("Nenhum caracter único foi encontrado.");
    }
    
}
